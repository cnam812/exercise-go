# Тестовое задание

### Задачи


##### 1 Квадратный мир

Мы живем в квадратном мире, у нас есть навигатор для построения маршрутов, но нам нужна функция 
проверки корректности этих маршрутов по кторым мы будем гулять. 

Все дома в нашем мире квадратные, пройти одну сторону дома мы можем за 1 минуту, 
наша прогулка должна быть 10 минут, на каждом перекрестке мы можем повеернуть на (север, юг, запад, восток),
массив этих значений и будет входным параметром нашей функции проверки.


Приммер:

Входной параметр:
```
{
   "north",
   "south",
   "west",
   "east",
   "north",
   "south",
   "west",
   "east",
   "west",
   "east",
}
```

Ответ: true


##### 2 Щиталка

Мы играем в детскую игру щиталку, где все дети содятся в круг, и ведущий начинает считать детей по порядку.

Нужно написать функцию, где, входные параметры:

1. массив детей
2. порядковый номер ребенка

в ответ она должна возвращать того ребенка на которого мы указали по порядковому номеру

Входной параметр:

```
{
    'a',
    'b',
    'c',
}
$number = 2;
```

Ответ: b
